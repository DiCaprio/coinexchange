package com.xxll.model;

import com.xxll.domain.SysMenu;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.List;

/**
 * 登录的返回值
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoginResult {

    /**
     * 登录产生的token,  由authorization-server服务器生成
     */
    private String token;

    /**
     * 前端的菜单数据
     */
    private List<SysMenu> menus;

    /**
     * 权限数据
     */
    private List<SimpleGrantedAuthority> authorities;
}
