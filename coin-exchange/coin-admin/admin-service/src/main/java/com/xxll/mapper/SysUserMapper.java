package com.xxll.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xxll.domain.SysUser;

public interface SysUserMapper extends BaseMapper<SysUser> {
}